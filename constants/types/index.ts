export interface Pizza{
    readonly id : number;
    readonly name : string;
    readonly basePrice : number;
    readonly cheeseCrust : boolean;
    readonly baseIngredients? : Ingredient[]; 
} 

export interface Ingredient{
    readonly id : number;
    readonly name : string;
    readonly price : number;
}

export interface PizzaOrder{
    readonly pizzaId : number;
    readonly pizzaName: string;
    readonly basePrice: number;
    readonly extrasTotal: number;
    readonly total : number;
    readonly quantity: number;
    readonly extraIngredients: Ingredient[];
}

export interface Order{
    readonly id : number;
    readonly subtotal: number;
    readonly total: number;
    readonly pizzas?: PizzaOrder[];
}

interface Data<T>{
    data: T[];
}

export type PizzaData = Data<Pizza>;
export type OrderData = Data<Order>;