import 'antd/dist/antd.css';
import '../styles/globals.css';
import React from 'react';

import PizzaProvider from '../components/contexts/Pizza.context';
import OrderProvider from '../components/contexts/Order.context';
import ErrorBoundary from '../components/atoms/ErrorBoundary';


const MyApp = ({ Component, pageProps }) => (
  <ErrorBoundary>
    <OrderProvider>
      <PizzaProvider>
        <Component {...pageProps} />
      </PizzaProvider>
    </OrderProvider>
  </ErrorBoundary>
);

export default MyApp
