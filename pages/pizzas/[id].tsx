import { useRouter } from 'next/router';
import { PageHeader, Descriptions, message } from 'antd';
import { usePizzaInformation } from '../../components/hooks/usePizzaInformation';
import GenericList from '../../components/organism/GenericList';
import { Ingredient } from '../../constants/types';
import { withPizzaContextInitialized } from '../../components/hoc';
import { IngredientListItem } from '../../components/molecules/IngredientDisplay';

const PizzaDetail = () => {
  const router = useRouter();
  const { data: pizzaInformation } = usePizzaInformation(
    parseInt(router.query?.id as string));

  if (!pizzaInformation) {
    message.error("The pizza doesn't exist redirecting back...", 2, () =>
      router.push('/orders')
    );
    return <></>;
  }

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Pizza"
        subTitle="Profile"
      >
        {pizzaInformation && (
          <Descriptions size="small" column={1}>
            <Descriptions.Item label="Name">{pizzaInformation.name}</Descriptions.Item>
            <Descriptions.Item label="Base Price">$ {pizzaInformation.basePrice}</Descriptions.Item>
            <Descriptions.Item label="CheeseCrust">{pizzaInformation.cheeseCrust}</Descriptions.Item>
          </Descriptions>
        )}
        <GenericList<Ingredient>
          loading={false}
          data={pizzaInformation && pizzaInformation.baseIngredients}
          ItemRenderer={({ item }: any) => <IngredientListItem item={item}/>}
        />
      </PageHeader>
    </>
  );
};

export default withPizzaContextInitialized(PizzaDetail);
