import React, { useEffect } from 'react'
import styled from 'styled-components';
import { Button, Layout, PageHeader, Col, Row } from 'antd';
import { useRouter } from 'next/dist/client/router';
import { usePizzas } from '../components/hooks/usePizzas';
import { CheckOutlined } from '@ant-design/icons';
import Loading from '../components/atoms/Loading';


const Image = styled.img`
  height: 44px;
  width: 95px;
  margin: 0 auto;
`;

const Splash = () =>{
  const router = useRouter();
  const { execute, initialized: initializedPizzaInfo } = usePizzas();
  
  useEffect(() => {
    (async () => {
      await execute();
    })();
  }, []);

  useEffect(() => {
    if (initializedPizzaInfo) router.push('/pizzas');
  }, []);

  return (
    <PageHeader
      subTitle="We are loading your information"
      extra={[
        <Button
          key="beon-logo"
          type="link"
          href="https://beon.studio/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src="/beon.svg" alt="BEON Logo" />
        </Button>,
      ]}
    >
      <Layout>
        <Layout.Content>
          <Row>
            <Col span={24}>
            {initializedPizzaInfo ? (
                <CheckOutlined />
              ) : (
                <Loading title="Loading Pizzas" />
              )}
            </Col>
          </Row>
        </Layout.Content>
      </Layout>
    </PageHeader>
  );

};

export default Splash;