import { useRouter } from "next/dist/client/router"
import GenericList from "../components/organism/GenericList";
import { withPizzaContextInitialized } from "../components/hoc";
import React, { useEffect } from "react";
import Header from "../components/molecules/ListHeader";
import PageHeader from "antd/lib/page-header";
import Layout from "../components/molecules/Layout";
import { useOrders } from "../components/hooks/useOrders";
import { OrderCard } from "../components/molecules/OrderDisplay";
import { Order } from "../constants/types";

const Orders = () => {
    const router = useRouter();
    const { data: orders, loading, execute } = useOrders();

    useEffect(() => {
        (async () => {
          await execute();
        })();
      }, []);
    
    return (
        <Layout>
            <PageHeader title="Orders">
                <GenericList<Order>
                    data={orders}
                    loading={loading}
                    ItemRenderer={({ item }: any) => (
                        <OrderCard item={item} />
                    )}
                    Header={
                        <Header
                            title={`Displaying ${orders.length} of ${orders.length}`}
                            pageSize={orders.length}
                        />
                    }
                />
            </PageHeader>
        </Layout>
    );
}

export default Orders;