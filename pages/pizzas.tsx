import { useRouter } from "next/dist/client/router"
import { usePizzas } from "../components/hooks/usePizzas";
import GenericList from "../components/organism/GenericList";
import { Pizza } from "../constants/types";
import { PizzaListItem } from '../components/molecules/PizzaDisplay';
import { withPizzaContextInitialized } from "../components/hoc";
import React from "react";
import Header from "../components/molecules/ListHeader";
import PageHeader from "antd/lib/page-header";
import Layout from "../components/molecules/Layout";

const Pizzas = () => {
    const router = useRouter();
    const { data: pizzas, loading } = usePizzas();
    ;
    return (
        <Layout>
            <PageHeader title="Pizza Menu">
                <GenericList<Pizza>
                    data={pizzas}
                    loading={loading}
                    ItemRenderer={({ item }: any) => (
                        <PizzaListItem onEdit={(id) => router.push(`/pizzas/${id}`)} item={item} />
                    )}
                    Header={
                        <Header
                            title={`Displaying ${pizzas.length} of ${pizzas.length}`}
                            pageSize={pizzas.length}
                        />
                    }
                />
            </PageHeader>
        </Layout>
    );
}

export default withPizzaContextInitialized(Pizzas);