import Axios from 'axios';
import { Ingredient, IngredientData, OrderData, PizzaData } from '../../constants/types';
import { APIGetIngredintsType, APIGetOrdersType, APIGetPizzasType } from './api.types';

const AxiosInstance = Axios.create({
    baseURL: 'http://localhost:4000',
    timeout: 200,
});

class ApiClient{
    async getPizzas() : Promise<PizzaData>{
        const {data} = await AxiosInstance.get<any, APIGetPizzasType>(
            `/pizzas`
        );
        ;
        return {data};
    }

    async getOrders() : Promise<OrderData>{
        const {data} = await AxiosInstance.get<any, APIGetOrdersType>(
            `/orders`
        );
        ;
        return {data};
    }
}


export default new ApiClient();