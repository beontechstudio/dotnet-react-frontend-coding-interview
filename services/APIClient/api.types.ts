import { AxiosResponse } from 'axios';
import { Ingredient, Order, Pizza } from '../../constants/types';


export interface APIData<T> {
    status: string;
    code: number;
    total: number;
    data: T;
  }
  
export type APIGetPizzasType = AxiosResponse<Pizza[]>;
export type APIGetOrdersType = AxiosResponse<Order[]>;