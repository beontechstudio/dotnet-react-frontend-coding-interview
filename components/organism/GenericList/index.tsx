import React from 'react';
import { List } from 'antd';
import InfiniteScroll from 'react-infinite-scroller';
import styled from 'styled-components';

interface Item {
  readonly id: number;
}

interface GenericListProps<T extends Item> {
  loading: boolean;
  Header?: React.ReactNode;
  data: T[];
  extra?: any;
  ItemRenderer: (params: { item: T }) => JSX.Element;
}

const Container = styled.div`
  max-height: 90vh;
  width: 100%;
  padding: 10px;
  overflow: auto;
  border-radius: 4px;
`;

const GenericList = <T extends Item>({
  data,
  loading,
  ItemRenderer,
  extra = {},
  Header,
}: GenericListProps<T>) => {
  ;
  return <Container>
    <InfiniteScroll
      initialLoad={false}
      pageStart={0}
      useWindow={false}
      loadMore={() => {}}
    >
      <List
        {...extra}
        header={Header}
        loading={loading}
        dataSource={data}
        renderItem={(item: T) => <ItemRenderer key={item.id} item={item} />}
      />
    </InfiniteScroll>
  </Container>
};

export default GenericList;
