import { createContext, useContext, useReducer } from "react";
import { Pizza } from "../../constants/types";
import { IContextData, IContextProviderProps } from "./types.context";

const EDIT_ACTION = 'edit';
const INITIALIZE_ACTION = 'initialize';

type ContextData = Pizza[];
interface PizzaState extends IContextData<ContextData> {
}

export interface IPizzaContext extends PizzaState {
  edit: (data: Pizza) => void;
  initialize: (data: Pizza[]) => void;
}

type Action =
  | { type: 'edit'; data: Pizza }
  | { type: 'initialize'; data: Pizza[] };

type Reducer = (prevState: PizzaState, action: Action) => PizzaState;

const reducer: Reducer = (state, action) => {
  ;
  switch (action.type) {
    case INITIALIZE_ACTION:
      return {
        ...state,
        data: action.data,
        initialized: true,
      };
    case EDIT_ACTION:
    case EDIT_ACTION:
      // this could have a better performance by storing a Map structure
      // cotaining the pizza's id as key and value their position
      //  on the array. That way the findIndex method is not necessary
      const index = state.data.findIndex(
        (pizza) => pizza.id === action.data.id
      );
      // The state should never be modifiied directly
      // Variant: Object.assign([], state.data, {[index]: newItem});
      const copy = state.data.slice(0);
      copy[index] = action.data;
      return { ...state, data: copy };
    default:
      throw new Error();
  }
};

export const PizzaContext = createContext<IPizzaContext>({
  data: [],
  initialized: false,
  edit: () => { },
  initialize: () => { },
});


export const usePizzaContext = () => useContext(PizzaContext);

const PizzaProvider = (({ children }: IContextProviderProps<ContextData>) => {
  const [state, dispatch] = useReducer(reducer, {
    data: [],
    initialized: false,
  });
  ;
  return (
    <PizzaContext.Provider
      value={{
        ...state,
        initialize: (data: Pizza[]) =>
          dispatch({ data, type: INITIALIZE_ACTION }),
        edit: (data: Pizza) => dispatch({ data, type: EDIT_ACTION }),
      }}
    >
      {children}
    </PizzaContext.Provider>

  );
});

export default PizzaProvider;