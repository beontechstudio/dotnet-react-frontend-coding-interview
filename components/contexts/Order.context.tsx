import { createContext, useContext, useReducer } from "react";
import { Order } from "../../constants/types";
import { IContextData, IContextProviderProps } from "./types.context";

const INITIALIZE_ACTION = 'initialize';

type ContextData = Order[];
interface OrderState extends IContextData<ContextData> {
}

export interface IOrderContext extends OrderState {
    initialize: (data: Order[]) => void;
}

type Action =
    | { type: 'initialize'; data: Order[] };

type Reducer = (prevState: OrderState, action: Action) => OrderState;

const reducer: Reducer = (state, action) => {
  ;
    switch (action.type) {
        case INITIALIZE_ACTION:
            return {
                ...state,
                data: action.data,
                initialized: true,
            };
        default:
            throw new Error();
    }
};

export const OrderContext = createContext<IOrderContext>({
    data: [],
    initialized: false,
    initialize: () => {},
});


export const useOrderContext = () => useContext(OrderContext);

const OrderProvider = (({children} : IContextProviderProps<ContextData>) => {
    const [state, dispatch] = useReducer(reducer, {
        data: [],
        initialized: false,
      });
    ;
      return (
        <OrderContext.Provider
        value={{
          ...state,
          initialize: (data: Order[]) =>
            dispatch({ data, type: INITIALIZE_ACTION }),
        }}
      >
        {children}
      </OrderContext.Provider>

      );
});

export default OrderProvider;