import React from 'react';
import { Space, Typography } from 'antd';


interface HeaderProps {
  title: string;
  pageSize: number;
  extra?: React.ReactNode[];
}

const Header = ({
  title,
  extra = [],
}: HeaderProps) => (
  <Space direction="horizontal">
    <Typography.Text>{title}</Typography.Text>
    {extra}
  </Space>
);

export default Header;
