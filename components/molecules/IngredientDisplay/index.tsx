import React from 'react';
import { List } from 'antd';
import { Ingredient } from '../../../constants/types';


interface IngredientDisplayProps {
  item: Ingredient;
}

export const IngredientListItem = ({ item }: IngredientDisplayProps) => {
  return <List.Item
    actions={[]}
  >
    <List.Item.Meta
      title={`${item.name} - Price: $${item.price}`}
    />
  </List.Item>
};


export default IngredientListItem;
