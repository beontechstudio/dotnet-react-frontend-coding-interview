import React from 'react';
import { List, Button, Card } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import styled from 'styled-components';

import { Pizza } from '../../../constants/types';

interface PizzaDisplayProps {
  item: Pizza;
  onEdit: (id: number) => void;
}

export const PizzaListItem = ({ onEdit, item }: PizzaDisplayProps) => {
  return <List.Item
    actions={[
      <Button onClick={() => onEdit(item.id)} icon={<EditOutlined />} />,
    ]}
  >
    <List.Item.Meta
      title={item.name}
    />
  </List.Item>
};


export default PizzaListItem;
