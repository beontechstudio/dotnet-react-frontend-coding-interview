import React from 'react';
import { List, Button, Card, Divider, Tag } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import styled from 'styled-components';

import { Order, PizzaOrder } from '../../../constants/types';

interface OrderDisplayProps {
  item: Order;
}

const CustomCard = styled(Card)`
  border: 1px solid rgba(106, 103, 103, 0.775);
`;

export const OrderCard = ({ item }: OrderDisplayProps) => (
  <List.Item>
    <CustomCard
      bordered
      title={`Order Id: ${item.id}`}
      style={{ width: 400 }}
      actions={[]}
    >
      {
        item.pizzas.map(x => <PizzaOrderDetail key={x.pizzaId} {...x} />)
      }
      <p>
        <i>Subtotal: ${item.subtotal}</i>
      </p>
      <p>
        <b>Total: ${item.total}</b>
      </p>
    </CustomCard>
  </List.Item>
);

export const PizzaOrderDetail = ({ pizzaName, extraIngredients, basePrice, extrasTotal, quantity, total }: PizzaOrder) => {
  return <>
    <Divider />
    <b>{pizzaName}</b><i> x {quantity}</i>
    <p>Base price: ${basePrice}</p>
    <p>Extra ingredients:</p>
    {
      extraIngredients.map((x, i) => <Tag key={i}>
        {x.name}: ${x.price}
      </Tag>)
    }
    <p>Extras total: ${extrasTotal}</p>
    <p>Pizza total: ${total}</p>
    <Divider />
  </>
};

export default OrderCard;
