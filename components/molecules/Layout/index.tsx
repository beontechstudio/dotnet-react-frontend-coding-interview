import Head from "next/head";
import React from "react";
import navButtons from "../../../config/buttons";
import NavBar from "../NavBar/NavBar";

const Layout = props => {
    return (
        <div className="Layout">
            <Head>
                <title>PizzaPlace</title>
            </Head>
            <NavBar navButtons={navButtons} />
            <div className="Content">{props.children}</div>
        </div>
    )
}

export default Layout;