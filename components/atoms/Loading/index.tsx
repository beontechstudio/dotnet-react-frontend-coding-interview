import React from 'react';
import { Spin } from 'antd';
import styled from 'styled-components';

const Container = styled.div`
  width: 100%;
  height: 100%;
  text-align: center;
`;

interface LoadingProps {
  title?: string;
}

const Loading = ({ title }: LoadingProps) => (
  <Container>
    <Spin
      style={{ margin: '0 auto' }}
      size="large"
      tip={title}
    />
  </Container>
);

export default Loading;
