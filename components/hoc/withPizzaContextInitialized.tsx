import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import { usePizzaContext } from '../contexts/Pizza.context';

export const withPizzaContextInitialized = (Component) => (props) => {
  const { initialized: initializedPizzas } = usePizzaContext();
  const router = useRouter();
  ;
  useEffect(() => {
    if (!initializedPizzas) router.push('/');
  }, [initializedPizzas]);

  return <Component {...props} />;
};
