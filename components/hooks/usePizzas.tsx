import { useCallback, useState } from "react";
import APIClient from "../../services/APIClient";
import { usePizzaContext } from "../contexts/Pizza.context";

export const usePizzas = () =>{
  ;
    const [error, setError] = useState();
    const [loading, setLoading] = useState(false);
    const pizzasData = usePizzaContext();

    const execute = useCallback(
        async () => {
          setLoading(true);
          try {
            ;
            const {data} = await APIClient.getPizzas();
            ;
            // if (pizzaData.initialized) pizzaData.append(data, totalItems);
            pizzasData.initialize(data);
          } catch (err) {
            setError(error);
          } finally {
            setLoading(false);
          }
        },
        [pizzasData.initialize]
      );
        ;
      return { ...pizzasData, execute, error, loading };
    };