import { useCallback, useState } from "react";
import APIClient from "../../services/APIClient";
import { useOrderContext } from "../contexts/Order.context";

export const useOrders = () =>{
  ;
    const [error, setError] = useState();
    const [loading, setLoading] = useState(false);
    const ordersData = useOrderContext();

    const execute = useCallback(
        async () => {
          setLoading(true);
          try {
            ;
            const {data} = await APIClient.getOrders();
            ;
            // if (pizzaData.initialized) pizzaData.append(data, totalItems);
            ordersData.initialize(data);
          } catch (err) {
            setError(error);
          } finally {
            setLoading(false);
          }
        },
        [ordersData.initialize]
      );
        ;
      return { ...ordersData, execute, error, loading };
    };