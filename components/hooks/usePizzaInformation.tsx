import { useState } from 'react';
import { Pizza } from '../../constants/types';
import { usePizzaContext } from '../contexts/Pizza.context';

export const usePizzaInformation = (id: number) => {
  const { data: pizzaData } = usePizzaContext();
  const [data] = useState<Pizza>(
    pizzaData.find((pizza) => pizza.id === id)
  );
  return { data };
};
