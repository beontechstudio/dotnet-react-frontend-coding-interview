import React from "react";
import { FormOutlined, PieChartOutlined } from '@ant-design/icons';

const navButtons = [
  {
    label: "Pizzas",
    path: "/pizzas",
    icon: <PieChartOutlined />
  },
  {
    label: "Orders",
    path: "/orders",
    icon: <FormOutlined />
  }
];

export default navButtons;