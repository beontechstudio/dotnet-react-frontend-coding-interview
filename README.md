This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Basic requirements

- Your preferred IDE / Code Editor
- NodeJS > 12
- Your preferred browser (tested on Chrome and Firefox)

## Folder structure

The components folder was designed usign Atomic design (atoms, molecules, organisms, templates). I also added some other folders that didn't fall under those categories: HOC (high order components, hooks and context). All the components under atoms, molecules, organisms and templates were designed in a way to not handle context-state or API logic in there (following Atomic Design principles and to make them flexible).

I created some custom hooks to handle API and state calls in a transparent way.

Other topic to consider is styling which was done in the vast majority using `styled-components` (less files and more inline with the new paradigm).

I used the NextJS server which has it's own router added so the pages are created under the special folder `pages` that has it's own structure (such as for param pages: `pages/pizzas/[id].tsx`, not found page, etc.)

In order to maintain state I created two contexts:

- Pizza context for pizza management.
- Order context for order management.

The service folder contains the APIClient (Singleton like component) that contains every aspect and dependency for API requests.
